<div class="wrap-icon-section minicart">
    <a href="{{ route('producto.cart') }}" class="link-direction">
        <i class="fa fa-shopping-basket" aria-hidden="true"></i>
        <div class="left-info">
            @if (Cart::instance('cart')->count() >= 0)
                <span class="index">{{ Cart::instance('cart')->count() }} Productos</span>
            @endif
            <span class="title">CARRITO</span>
        </div>
    </a>
</div>
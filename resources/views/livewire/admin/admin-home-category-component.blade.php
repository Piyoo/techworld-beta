<div>
    <div class="container" style="padding: 30px 0px;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-uppercase">
                                <h5>Todas las Categorias</h5>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.addcategoria') }}" class="btn btn-success pull-right">Nueva Categoria</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <form class="form-horizontal container-form" wire:submit.prevent="actualizarHomeCategoria">
                            <div class="mb-3 from-group mt" wire:ignore>
                                <label class="form-label mb-3">Elige las categorias</label>
                                <select class="sel_categorias form-control form-select-lg mb-3" name="categorias[]" multiple="multiple" wire:model="categorias_seleccionadas">
                                    @foreach ($categorias as $categoria)
                                        <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-3 from-group mt">
                                <label class="form-label mb-3">Numero de Productos</label>
                                <input type="text" class="form-control" wire:model="numeroProductos">
                            </div>

                            <button type="submit" class="btn btn-primary mt">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .container-form{
            margin: 40px !important;
            padding: 0px 100px !important;
        }
    
        .mt{
            margin-top: 20px !important;
        }
    </style>
</div>

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.sel_categorias').select2();
            $('.sel_categorias').on('change',function(e){
                var data = $('.sel_categorias').select2("val");
                @this.set('categorias_seleccionadas',data);
            });
        });
    </script>
@endpush
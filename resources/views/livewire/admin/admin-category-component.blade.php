<div>
    <style>
        nav svg{
            height: 20px;
        }
        nav .hidden{
            display: block !important;
        }
    </style>
    <div class="container" style="padding: 30px 0px;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-uppercase">
                                <h5>Todas las Categorias</h5>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.addcategoria') }}" class="btn btn-success pull-right">Nueva Categoria</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Slug</th>
                                    <th style="width: 300px !important;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categorias as $categoria)
                                    <tr>
                                        <td>{{ $categoria->id }}</td>
                                        <td>{{ $categoria->nombre }}</td>
                                        <td>{{ $categoria->slug }}</td>
                                        <td>
                                            <a href="{{ route('admin.editcategoria',['categoria_slug'=>$categoria->slug]) }}">
                                                <button type="button" class="btn btn-success">
                                                    <i class="fa fa-edit fa-1x"></i>
                                                    Editar
                                                </button>
                                            </a>
                                            <a href="#" onclick="confirm('¿Estas seguro de querer eliminar esta categoria?') || event.stopImmediatePropagation()" wire:click.prevent="eliminarCategoria({{ $categoria->id }})" style="margin-left: 15px;">
                                                <button type="button" class="btn btn-danger">
                                                    <i class="fa fa-times fa-1x"></i>
                                                    Eliminar
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $categorias->links() }}
            </div>
        </div>
    </div>
</div>

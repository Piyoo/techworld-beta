<div>
    <style>
        nav svg{
            height: 20px;
        }
        nav .hidden{
            display: block !important;
        }
    </style>
    <div class="container" style="padding: 30px 0px;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-uppercase">
                                <h5>Todas los Productos</h5>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.addproducto') }}" class="btn btn-success pull-right">Nuevo producto</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Imagen</th>
                                    <th>Nombre</th>
                                    <th>Stock</th>
                                    <th>Precio</th>
                                    <th>Categoria</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($productos as $producto)
                                    <tr>
                                        <td>{{ $producto->id }}</td>
                                        <td><img src="{{ asset('assets/images/products') }}/{{ $producto->imagen }}" alt="{{ $producto->nombre }}" width="60px"></td>
                                        <td>{{ $producto->nombre }}</td>
                                        <td>{{ $producto->stock_estatus }}</td>
                                        <td>$ {{ $producto->precio_regular }}</td>
                                        <td>{{ $producto->categoria->nombre }}</td>
                                        <td>{{ $producto->created_at }}</td>
                                        <td>
                                            <a href="{{ route('admin.editproducto',['producto_slug'=>$producto->slug]) }}">
                                                <button type="button" class="btn btn-success">
                                                    <i class="fa fa-edit fa-1x"></i>
                                                    Editar
                                                </button>
                                            </a>
                                            <a href="#" onclick="confirm('¿Estas seguro de querer eliminar esta producto?') || event.stopImmediatePropagation()" wire:click.prevent="eliminarProducto({{ $producto->id }})" style="margin-left: 15px;">
                                                <button type="button" class="btn btn-danger">
                                                    <i class="fa fa-times fa-1x"></i>
                                                    Eliminar
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $productos->links() }}
            </div>
        </div>
    </div>
</div>

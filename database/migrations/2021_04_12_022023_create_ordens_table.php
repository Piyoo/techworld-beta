<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordens', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('usuario_id')->unsigned();
            $table->decimal('subtotal');
            $table->decimal('descuento')->default(0);
            $table->decimal('iva');
            $table->decimal('total');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('telefono');
            $table->string('email');
            $table->string('line1');
            $table->string('line2')->nullable();
            $table->string('ciudad');
            $table->string('provincia');
            $table->string('pais');
            $table->string('codigopostal');
            $table->enum('estatus',['ordenado','entregado','cancelado'])->default('ordenado');
            $table->boolean('envio_diferente')->default(false);
            $table->timestamps();
            $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordens');
    }
}

<?php

namespace Database\Factories;

use App\Models\Producto;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Producto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $producto_nombre = $this->faker->unique()->words($nb=4,$asText=true);
        $slug = Str::slug($producto_nombre);
        return [
            'nombre' => $producto_nombre,
            'slug' => $slug,
            'descripcion_corta' => $this->faker->text(200),
            'descripcion' => $this->faker->text(500),
            'precio_regular' => $this->faker->numberBetween(10, 500),
            'SKU' => 'DIGI' .$this->faker->unique()->numberBetween(100,500),
            'stock_estatus' => 'instock',
            'cantidad' => $this->faker->numberBetween(100,200),
            'imagen' => 'digital_' .$this->faker->unique()->numberBetween(1,22).'.jpg',
            'categoria_id' => $this->faker->numberBetween(1,5)
        ];
    }
}

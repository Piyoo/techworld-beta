<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
    use HasFactory;

    protected $table = "ordens";

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ordenItems()
    {
        return $this->hasMany(OrdenItems::class);
    }

    public function envio()
    {
        return $this->hasOne(Envio::class);
    }

    public function transaccion()
    {
        return $this->hasOne(Transaccion::class);
    }
}

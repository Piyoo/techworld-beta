<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdenItem extends Model
{
    use HasFactory;

    protected $table = "orden_items";

    public function orden()
    {
        return $this->belongsTo(Orden::class);
    }
}

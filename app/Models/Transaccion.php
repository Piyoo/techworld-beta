<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
    use HasFactory;

    protected $table = "transaccions";

    public function orden()
    {
        return $this->belongsTo(Orden::class);
    }
}

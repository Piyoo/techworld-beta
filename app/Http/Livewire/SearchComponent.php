<?php

namespace App\Http\Livewire;

use App\Models\Producto;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Categoria;
use Cart;

class SearchComponent extends Component
{
    public $clasificacion;
    public $cantidad;

    public $busqueda;
    public $producto_cat;
    public $producto_cat_id;

    //Acomodo de la vista de la tienda
    public function mount()
    {
        $this->clasificacion = "default";
        $this->cantidad = 12;
        $this->fill(request()->only('busqueda','producto_cat','producto_cat_id'));
    }

    //Almacena los productos en el carrito
    public function store($producto_id,$producto_nombre,$producto_precio)
    {
        Cart::add($producto_id,$producto_nombre,1,$producto_precio)->associate('App\Models\Producto');
        session()->flash('success_message','Producto agregado al carrito');
        return redirect()->route('producto.cart');
    }

    use WithPagination;
    public function render()
    {
        //Clasificación de los productos
        if ($this->clasificacion == 'date')
        {
            $productos = Producto::where('nombre','like','%'.$this->busqueda.'%')->where('categoria_id','like','%'.$this->producto_cat_id.'%')->orderBy('created_at', 'DESC')->paginate($this->cantidad);
        }
        else if($this->clasificacion == 'price')
        {
            $productos = Producto::where('nombre','like','%'.$this->busqueda.'%')->where('categoria_id','like','%'.$this->producto_cat_id.'%')->orderBy('precio_regular', 'ASC')->paginate($this->cantidad);
        }
        else if ($this->clasificacion == 'price-desc')
        {
            $productos = Producto::where('nombre','like','%'.$this->busqueda.'%')->where('categoria_id','like','%'.$this->producto_cat_id.'%')->orderBy('precio_regular', 'DESC')->paginate($this->cantidad);
        }
        else
        {
            $productos = Producto::where('nombre','like','%'.$this->busqueda.'%')->where('categoria_id','like','%'.$this->producto_cat_id.'%')->paginate($this->cantidad);
        }

        $categorias = Categoria::all();

        return view('livewire.search-component',['productos' => $productos, 'categorias' => $categorias])->layout('layouts.base');
    }
}

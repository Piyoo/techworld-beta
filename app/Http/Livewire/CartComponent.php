<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use App\Models\Cupon;
use Cart;

class CartComponent extends Component
{
    public $tengoCodigoCupon;
    public $codigoCupon;
    public $descuento;
    public $subtotalDespuesDescuento;
    public $ivaDespuesDescuento;
    public $totalDespuesDescuento;

    //Aunmenta la cantidad del producto
    public function incrementa($rowId)
    {
        $producto = Cart::instance('cart')->get($rowId);
        $qty = $producto->qty + 1;
        Cart::instance('cart')->update($rowId,$qty);
        $this->emitTo('cart-cound-component','refreshComponent');
    }

    //Disminuye la cantidad del producto
    public function decrementa($rowId)
    {
        $producto = Cart::instance('cart')->get($rowId);
        $qty = $producto->qty - 1;
        Cart::instance('cart')->update($rowId,$qty);
        $this->emitTo('cart-cound-component','refreshComponent');
    }

    //Elimina objetos del carrito de compras
    public function destroy($rowId)
    {
        Cart::instance('cart')->remove($rowId);
        $this->emitTo('cart-cound-component','refreshComponent');
        session()->flash('success_message', 'Producto removido del carrito');
    }

    //Elimina todo el contenido del Carrito de compras
    public function destroyAll()
    {
        Cart::instance('cart')->destroy();
        $this->emitTo('cart-cound-component','refreshComponent');   
        session()->flash('success_message', 'Carrito de compras en blanco');
    }

    //Aplicar descuento de cupón
    public function aplicarCodigoCupon()
    {
        $cupon = Cupon::where('codigo', $this->codigoCupon)->where('cart_valor','<=',Cart::instance('cart')->subtotal())->first();
        if (!$cupon)
        {
            session()->flash('cupon_message','El cupón es invalido'.$cupon.' eso');
            return;
        }

        session()->put('cupon',[
            'codigo' => $cupon->codigo,
            'tipo' => $cupon->tipo,
            'valor' => $cupon->valor,
            'cart_valor' => $cupon->cart_valor
        ]);
        
    }

    public function calcularDescuento()
    {
        if (session()->has('cupon'))
        {
            if (session()->get('cupon')['tipo'] == 'fixed')
            {
                $this->descuento = session()->get('cupon')['valor'];
            }
            else
            {
                $this->descuento = (Cart::instance('cart')->subtotal() * session()->get('cupon')['valor'])/100;
            }
            $this->subtotalDespuesDescuento = Cart::instance('cart')->subtotal() - $this->descuento;
            $this->ivaDespuesDescuento = ($this->subtotalDespuesDescuento * config('cart.tax'))/100;
            $this->totalDespuesDescuento = $this->subtotalDespuesDescuento + $this->ivaDespuesDescuento;
        }
    }

    public function quitarCupon()
    {
        session()->forget('cupon');
    }

    public function checkout()
    {
        if (Auth::check())
        {
            return redirect()->route('checkout');
        }
        else
        {
            return redirect()->route('login');
        }
    }

    public function setAmountForCheckout()
    {
        if (!Cart::instance('cart')->count() > 0)
        {
            session()->forget('checkout');
            return;
        }
        
        if (session()->has('cupon'))
        {
            session()->put('checkout',[
                'descuento' => $this->descuento,
                'subtotal' => $this->subtotalDespuesDescuento,
                'iva' => $this->ivaDespuesDescuento,
                'total' => $this->totalDespuesDescuento
            ]);
        }
        else
        {
            session()->put('checkout',[
                'descuento' => 0,
                'subtotal' => Cart::instance('cart')->subtotal(),
                'iva' => Cart::instance('cart')->tax(),
                'total' => Cart::instance('cart')->total()
            ]);
        }
    }

    public function render()
    {
        if (session()->has('cupon'))
        {
            if (Cart::instance('cart')->subtotal() < session()->get('cupon')['cart_valor'])
            {
                session()->forget('cupon');
            }
            else
            {
                $this->calcularDescuento();
            }
        }
        $this->setAmountForCheckout();
        return view('livewire.cart-component')->layout('layouts.base');
    }
}

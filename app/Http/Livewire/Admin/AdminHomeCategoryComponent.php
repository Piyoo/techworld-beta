<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Categoria;
use App\Models\HomeCategoria;

class AdminHomeCategoryComponent extends Component
{
    public $categorias_seleccionadas = [];
    public $numeroProductos;

    public function mount()
    {
        $categoria = HomeCategoria::find(1);
        $this->categorias_seleccionadas = explode(',',$categoria->sel_categorias);
        $this->numeroProductos = $categoria->no_productos;
    }

    public function actualizarHomeCategoria()
    {
        $categoria = HomeCategoria::find(1);
        $categoria->sel_categorias = implode(',',$this->categorias_seleccionadas);
        $categoria->no_productos = $this->numeroProductos;
        $categoria->save();
        session()->flash('message','Las categorias del Home han sido actualizadas');
    }

    public function render()
    {
        $categorias = Categoria::all();
        return view('livewire.admin.admin-home-category-component',['categorias'=>$categorias])->layout('layouts.base');
    }
}

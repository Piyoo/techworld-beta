<?php

namespace App\Http\Livewire\Admin;

use Carbon\Carbon;
use Livewire\Component;
use App\Models\Producto;
use App\Models\Categoria;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class AdminEditProductComponent extends Component
{
    use WithFileUploads;
    public $nombre;
    public $slug;
    public $descripcion_corta;
    public $descripcion;
    public $precio_regular;
    public $precio_venta;
    public $SKU;
    public $stock_estatus;
    public $caracteristecas;
    public $cantidad;
    public $imagen;
    public $categoria_id;
    public $nuevaImagen;
    public $producto_id;

    public function mount($producto_slug)
    {
        $producto = Producto::where('slug',$producto_slug)->first();
        $this->nombre = $producto->nombre;
        $this->slug = $producto->slug;
        $this->descripcion_corta = $producto->descripcion_corta;
        $this->descripcion = $producto->descripcion;
        $this->precio_regular = $producto->precio_regular;
        $this->precio_venta = $producto->precio_venta;
        $this->SKU = $producto->SKU;
        $this->stock_estatus = $producto->stock_estatus;
        $this->caracteristecas = $producto->caracteristecas;
        $this->cantidad = $producto->cantidad;
        $this->imagen = $producto->imagen;
        $this->categoria_id = $producto->categoria_id;
        $this->producto_id = $producto->id;
    }

    //Aunto completa el slug
    public function generarSlug()
    {
        $this->slug = Str::slug($this->nombre.'-');
    }

    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'nombre' => 'required',
            'slug' => 'required|unique:productos',
            'descripcion_corta' => 'required',
            'descripcion' => 'required',
            'precio_regular' => 'required|numeric',
            'precio_venta' => 'numeric',
            'SKU' => 'required',
            'stock_estatus' => 'required',
            'cantidad' => 'required|numeric',
            'nuevaImagen' => 'required|mimes:jpeg,jpg,png',
            'categoria_id' => 'required'
        ]);
    }

    //Actualizar la informacion del Producto
    public function actualizarProducto()
    {
        $this->validate([
            'nombre' => 'required',
            'slug' => 'required|unique:productos',
            'descripcion_corta' => 'required',
            'descripcion' => 'required',
            'precio_regular' => 'required|numeric',
            'precio_venta' => 'numeric',
            'SKU' => 'required',
            'stock_estatus' => 'required',
            'cantidad' => 'required|numeric',
            'nuevaImagen' => 'required|mimes:jpeg,jpg,png',
            'categoria_id' => 'required'
        ]);
        $producto = Producto::find($this->producto_id);
        $producto->nombre = $this->nombre;
        $producto->slug = $this->slug;
        $producto->descripcion_corta = $this->descripcion_corta;
        $producto->descripcion = $this->descripcion;
        $producto->precio_regular = $this->precio_regular;
        $producto->precio_venta = $this->precio_venta;
        $producto->SKU = $this->SKU;
        $producto->stock_estatus = $this->stock_estatus;
        $producto->caracteristecas = $this->caracteristecas;
        $producto->cantidad = $this->cantidad;
        if ($this->nuevaImagen)
        {
            $imagenNombre = Carbon::now()->timestamp. '.' .$this->nuevaImagen->extension();
            $this->nuevaImagen->storeAs('products',$imagenNombre);
            $producto->imagen = $imagenNombre;
        }
        $producto->categoria_id = $this->categoria_id;
        $producto->save();
        session()->flash('message','El producto se ha actualizado exitosamente');
    }

    public function render()
    {
        $categorias = Categoria::all();
        return view('livewire.admin.admin-edit-product-component',['categorias'=>$categorias])->layout('layouts.base');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Producto;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Categoria;
use Cart;

class ShopComponent extends Component
{
    public $clasificacion;
    public $cantidad;

    //Acomodo de la vista de la tienda
    public function mount()
    {
        $this->clasificacion = "default";
        $this->cantidad = 12;
    }

    //Almacena los productos en el carrito
    public function store($producto_id,$producto_nombre,$producto_precio)
    {
        Cart::instance('cart')->add($producto_id,$producto_nombre,1,$producto_precio)->associate('App\Models\Producto');
        session()->flash('success_message','Producto agregado al carrito');
        return redirect()->route('producto.cart');
    }

    use WithPagination;
    public function render()
    {
        //Clasificación de los productos
        if ($this->clasificacion == 'date')
        {
            $productos = Producto::orderBy('created_at', 'DESC')->paginate($this->cantidad);
        }
        else if($this->clasificacion == 'price')
        {
            $productos = Producto::orderBy('precio_regular', 'ASC')->paginate($this->cantidad);
        }
        else if ($this->clasificacion == 'price-desc')
        {
            $productos = Producto::orderBy('precio_regular', 'DESC')->paginate($this->cantidad);
        }
        else
        {
            $productos = Producto::paginate($this->cantidad);
        }

        $categorias = Categoria::all();

        return view('livewire.shop-component',['productos' => $productos, 'categorias' => $categorias])->layout('layouts.base');
    }
}
